'use strict'
const {resolve} = require('path');

module.exports = {
    entry: './client/index.js',
  output: {
    path: __dirname,
    filename: './public/js/bundle.js'
    },
  // resolve: {
  //   extensions: ['.js', '.jsx']
  // },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: resolve(__dirname, './client'),
        loader: 'babel-loader',
        options: {
          // presets: ['react', 'es2015', 'stage-2']
          presets: ['@babel/preset-react', '@babel/preset-env']
        }
      }
    ]
  }
};

